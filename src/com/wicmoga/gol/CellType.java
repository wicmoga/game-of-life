package com.wicmoga.gol;

import java.util.Random;

public class CellType {
	
	public double rF;	//repulsion force strength to avoid collisions
	public double d;	//distance of the repulsion force
	
	public double h;	//force of attraction/repulsion
	public double l;	//distance of h
	
	public int color;
	
	public double mass;
	
	public static CellType[] TYPES;
	
	private CellType(double rF, double d, double h, double l, int color, double mass) {
		this.rF = rF;
		this.d = d;
		this.h = h;
		this.l = l;
		this.color = color;
		this.mass = mass;
	}
	
	public static void gen(double[][] cells) {
		TYPES = new CellType[cells.length];
		for(int i = 0; i < cells.length; i++) {
			TYPES[i] = new CellType(cells[i][0], cells[i][1],cells[i][2],cells[i][3],(int)cells[i][4],cells[i][5]);
		}
	}
	
	public static void randomGen(int n, double maxRF, double maxD, double maxH, double maxL, double maxMass) {
		TYPES = new CellType[n];
		Random r = Window.r;
		for(int i = 0; i < n; i++) {
			double rF = (r.nextDouble()+1)*maxRF/3;
			double d = (Math.random()*maxD+maxD);
			double h = (r.nextDouble()*2-1)*maxH/4;
			double l  = r.nextDouble()*maxL;
			double mass = (r.nextDouble()+1)*maxMass/1.5;
			int col = r.nextInt(0x1000000);
			TYPES[i] = new CellType(rF, d, h, l,col, mass);
		}
	}
	
	public String toString() {
		return "Color: " + Integer.toHexString(color) + "\n\tRepulsion force: " + rF + "\n\tRepulsion distance: " + d + "\n\tAction force: " + h + "\n\tAction distance: " + l + "\n\tMass: " + mass;
	}
}
