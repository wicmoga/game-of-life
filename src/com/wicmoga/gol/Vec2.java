package com.wicmoga.gol;

public class Vec2 {

	public double x, y;

	public Vec2() {
		this.x = 0.0;
		this.y = 0.0;
	}
	
	public Vec2(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public static Vec2 add(Vec2 v1, Vec2 v2) {
		return new Vec2(v1.x+v2.x, v1.y+v2.y);
	}

	public static Vec2 dot(Vec2 v1, Vec2 v2) {
		return new Vec2(v1.x*v2.x, v1.y*v2.y);
	}

	public static Vec2 diff(Vec2 v1, Vec2 v2) {
		return new Vec2(v1.x-v2.x, v1.y-v2.y);
	}
	
	public static Vec2 scale(Vec2 v, double k) {
		return new Vec2(k*v.x, k*v.y);
	}
	
	public static Vec2 div(Vec2 v, double k) {
		return scale(v, 1.0/k);
	}
	
	public static double distSq(Vec2 v1, Vec2 v2) {
		return Math.pow(v1.x-v2.x,2) + Math.pow(v1.y-v2.y,2);
	}
	
	public static double dist(Vec2 v1, Vec2 v2) {
		return Math.sqrt(distSq(v1, v2));
	}
	
	public static double modSq(Vec2 v) {
		return Math.pow(v.x,2) + Math.pow(v.y,2);
	}
	
	public static double mod(Vec2 v) {
		return Math.sqrt(modSq(v));
	}
	
	public static Vec2 norm(Vec2 v) {
		return Vec2.scale(v, mod(v));
	}
	
	public int getX() {
		return (int) x;
	}
	
	public int getY() {
		return (int) y;
	}
	
	public String toString() {
		return "x: "+ x + ", y: " + y;
	}
}
