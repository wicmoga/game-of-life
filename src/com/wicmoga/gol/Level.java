package com.wicmoga.gol;

import java.awt.Graphics;
import java.util.ArrayList;

public class Level {
	
	public int width, height;
	
	public double maxfriction = 10;
	public double friction = 0.2;
	
	public boolean wrap = false;
	
	public ArrayList<Cell> cells = new ArrayList<Cell>();
	
	public Level(int width, int height, boolean wrap) {
		this.width = width;
		this.height = height;
		this.wrap = wrap;
	}
	
	public void tick(double dt) {
		for (Cell c1 : cells) {
			for (Cell c2 : cells) {
				if(c1.equals(c2)) continue;
				if(Vec2.distSq(c1.pos, c2.pos) >= Window.maxIDSq) continue;
				c1.interact(c2);
			}
		}
		for (Cell c1 : cells) {
			c1.tick(dt);
		}
	}
	
	public void render(Graphics g) {
		for(Cell c : cells) {
			c.render(g, 0, 0);
		}
	}
	
	public void generateCells(int n) {
		for(int i = 0; i < n; i++) {
			CellType ct = CellType.TYPES[Window.r2.nextInt(CellType.TYPES.length)];
			Vec2 pos = new Vec2(Window.r2.nextDouble()*width,Window.r2.nextDouble()*height);
			cells.add(new Cell(ct, pos, new Vec2(), new Vec2()));
		}
	}
	
	public static Vec2 collide(Cell c) {
		
		return null;
	}
}
