package com.wicmoga.gol;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Window extends Canvas {
	
	public static int WIDTH = 1000, HEIGHT = 700;
	public BufferedImage image = new BufferedImage(WIDTH,HEIGHT,BufferedImage.TYPE_INT_RGB);
	public Thread thread;
	public static Rectangle border = new Rectangle(0, 0, WIDTH, HEIGHT);
	public static Level level;
	
	public Window() {
		JFrame jf = new JFrame("Los muerto");
		jf.setSize(WIDTH+30, HEIGHT+60);
		jf.setVisible(true);
		jf.add(this);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setLocationRelativeTo(null);
		jf.setResizable(true);
		
		gui(jf);
		
		thread = new Thread(()->run(),"Main");
		thread.start();
	}
	
	public void gui(JFrame jf) {
		JSlider frictionSlider = new JSlider(JSlider.VERTICAL, 0, 100, 10);
		frictionSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				level.friction = level.maxfriction*frictionSlider.getValue()/100.0;
			}
		});
		jf.add(frictionSlider, BorderLayout.EAST);
		
	}
	
	public void run() {
		init();
		long last = System.nanoTime();
		
		double UPS = 120.0D;
		double delta = 0;
		double ns = 1e9/UPS;
		
		while(true) {
			long now = System.nanoTime();
			delta += (now-last)/ns;
			last = now;
			
			while(delta >= 1) {
				delta--;
				tick(1/60.0D);
			}
			render();
		}
	}
	
	public static double maxL = 120;
	public static double maxD = 15;
	public static double maxIDSq = Math.pow(maxD + maxL*2,2);
	public static Random r;
	public static Random r2 = new Random();
	
	public void init() {
		long seed = r2.nextLong();
		r = new Random(seed);
		System.out.println(seed);
		level = new Level(WIDTH, HEIGHT, true);
		CellType.randomGen(7, 5000, maxD, 2, maxL, 20);
		level.generateCells(200);
		for(CellType type : CellType.TYPES) {
			System.out.println(type);
		}
//		level.cells.add(new Cell(CellType.TYPES[0], new Vec2(100,100), new Vec2(40,0), new Vec2()));
	}
	
	public void tick(double dt) {
		level.tick(dt);
	}
	
	public void render() {
		BufferStrategy b = getBufferStrategy();
		if(b==null) {
			createBufferStrategy(3);
			return;
		}
		
		Graphics g = b.getDrawGraphics();
		
		g.drawImage(image, 0, 0, null);
		
		level.render(g);
		g.dispose();
		b.show();
	}
}