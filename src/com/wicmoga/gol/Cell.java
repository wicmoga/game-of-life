package com.wicmoga.gol;

import java.awt.Color;
import java.awt.Graphics;

public class Cell {
	
	public CellType type;
	public Vec2 pos;
	public Vec2 vel;
	public Vec2 accel;
	
	public static final int size = 15;
	
	public Cell(CellType ct, Vec2 pos, Vec2 vel, Vec2 accel) {
		this.type = ct;
		this.pos = pos;
		this.vel = vel;
		this.accel = accel;
	}
	
	public void tick(double dt) {
		
		if(Vec2.modSq(vel) != 0) {
			Vec2 friction = Vec2.scale(Vec2.norm(vel), -Window.level.friction*type.mass);
			accel = Vec2.add(accel, Vec2.scale(friction,dt));
		}
		
		pos = Vec2.add(pos, Vec2.scale(vel, dt));
		
		collision();
		
		vel = Vec2.add(vel, Vec2.scale(accel, dt));
		accel.x = 0;
		accel.y = 0;
	}
	
	public void collision() {
		if(Window.level.wrap) {
			pos.x %= Window.level.width;
			pos.y %= Window.level.height;
			return;
		}
		if(pos.x+size/2 > Window.border.getMaxX()) {
			pos.x = Window.border.getMaxX();
			vel.x = - vel.x;
		} else if(pos.x - size/2 < Window.border.getMinX()) {
			pos.x = Window.border.getMinX();
			vel.x = - vel.x;
		}
		if(pos.y + size/2 > Window.border.getMaxY()) {
			pos.y = Window.border.getMaxY();
			vel.y = - vel.y;
		} else if(pos.y - size/2 < Window.border.getMinY()) {
			pos.y = Window.border.getMinY();
			vel.y = - vel.y;
		}
	}
	
	public void interact(Cell cell2) {
		double distance = Vec2.dist(pos, cell2.pos);
		double force = -Math.max(0, type.rF*(type.d-distance)*(type.d+distance+2)/(type.d*(type.d+2)*(distance+1))) 
				- force(distance);
		force /= type.mass;
		Vec2 ac = Vec2.scale(Vec2.norm(Vec2.diff(cell2.pos, pos)), force);
		accel = Vec2.add(accel, ac);
	}
	
	public double force(double distance) {
		if(type.h > 0) return Math.max(0, Math.min(type.h/type.l*distance-type.d,-type.h/type.l*distance-type.d-2*type.l));;
		return Math.min(0, Math.max(type.h/type.l*(distance-type.d),-type.h/type.l*(distance-type.d-2*type.l)));
	}
	
	public void render(Graphics g, double xo, double yo) {
		g.setColor(new Color(type.color));
		g.fillOval((int)(pos.x-xo-size/2.0), (int)(pos.y-yo-size/2.0), size, size);
	}
}